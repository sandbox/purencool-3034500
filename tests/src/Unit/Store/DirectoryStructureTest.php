<?php

namespace Drupal\react_forms;

use \Drupal\react_forms\Store\Directories\DirectoryStructure;


/**
 *
 * @group react_forms
 */
class DirectoryStructureTest extends \Drupal\Tests\UnitTestCase {

  /**
   * @var object
   */
  private $objToTest;


  /**
   *
   */
  public function setUp() {
    $this->objToTest = \Drupal::service('react_forms.react_forms_services');
  }


  /**
   *
   */
  public function testGetDirectories() {
    $this->assertEquals($this->objToTest->getDirectories()['base'], '/settings');
    fwrite(STDERR, print_r($this->objToTest->getDirectories(), TRUE));
  }


  /**
   * Once test method has finished running, whether it succeeded or failed,
   * tearDown() will be invoked. Unset the unit created object.
   */
  public function tearDown() {
    unset($this->objToTest);
  }
}






