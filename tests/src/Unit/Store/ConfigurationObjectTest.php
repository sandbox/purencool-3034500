<?php

namespace Drupal\Tests\react_forms\Unit\Store;


use Drupal\react_forms\Store\ConfigurationObject;


  /**
 *
 * @group react_forms
 */
class ConfigurationObjectTest extends \Drupal\Tests\UnitTestCase {

  /**
   * @var object
   */
  private $objToTest;


  /**
   *
   */
  public function setUp() {
    $this->objToTest =  new ConfigurationObject();
  }


  /**
   *
   */
  public function testGetDirectories() {
    $this->assertEquals( $this->objToTest->getConfig(), '/settings');
    fwrite(STDERR, print_r($this->objToTest->getConfig(), TRUE));
  }


  /**
   * Once test method has finished running, whether it succeeded or failed,
   * tearDown() will be invoked. Unset the unit created object.
   */
  public function tearDown() {
    unset($this->objToTest);
  }
}






