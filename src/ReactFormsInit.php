<?php
namespace Drupal\react_forms;

use Drupal\react_forms\Store\ConfigurationObject;
use Drupal\react_forms\ReactBuildTool\Utilities\Disk\DirectoryStructureCreation;
use Drupal\react_forms\ReactBuildTool\ReactBuildTool;



  /**
 * Class ConfigurationObject interface to the Drupal Layer
 *
 * @package Drupal\react_forms\Store
 */
class ReactFormsInit{


  /**
   * @return \Drupal\react_forms\Store\ConfigurationObject
   */
  private static function configurationObj(){
    return new ConfigurationObject();
  }

  /**
   * Rebuilds the React and React Native files and directory structure
   */
  public static function buildAllForms(){
    $obj = new ReactBuildTool();
    $obj->buildReact(
      self::configurationObj()->getConfig()['directory']->getDirectories(
        self::configurationObj()->getConfig()['forms_array']->getFormsArray()
      ),
      self::configurationObj()->getConfig()['file_extension'],
      self::configurationObj()->getConfig()['forms_array']->getFormsArray());

    $obj->buildReactNative(
      self::configurationObj()->getConfig()['directory']->getDirectories(
        self::configurationObj()->getConfig()['forms_array']->getFormsArray()
      ),
      self::configurationObj()->getConfig()['file_extension'],
      self::configurationObj()->getConfig()['forms_array']->getFormsArray());

  }


  /**
   * Creates directory structure for react forms to be saved in
   */
  public static function createDirectories(){
    $forms = self::configurationObj()->getConfig()['forms_array']->getFormsArray();
    $directoryCreation = new DirectoryStructureCreation();
    $directoryCreation->setDirectories(self::configurationObj()->getConfig()['directory']->getDirectories($forms));
    $directoryCreation->createDirectories();
  }


  /**
   * @todo
   */
  public static function deleteDirectories(){
    $directoryCreation = new DirectoryStructureCreation();
    $directoryCreation->setDirectories(self::configurationObj()->getConfig()['directory']->getDirectories());
    //$directoryCreation->deleteDirectories();
  }


  /**
   * @return string
   */
  public static function directory(){
    $forms = self::configurationObj()->getConfig()['forms_array']->getFormsArray();
    return self::configurationObj()->getConfig()['directory']->getDirectories($forms)['root']['dir_name'];
  }

}
