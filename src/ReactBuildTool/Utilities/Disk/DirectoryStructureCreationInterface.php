<?php

namespace Drupal\react_forms\ReactBuildTool\Utilities\Disk;

/**
 * Interface DirectoryStructureCreationInterface
 *
 * @package Drupal\webform_react_components\ReactBuildTool\Utilities\Disk
 */
interface DirectoryStructureCreationInterface {


  /**
   *
   * @return null
   */
  public function createDirectories();


  /**
   * @return null
   */
  public function deleteDirectories();


  /**
   * @param array $directoryArray
   * @return null
   */
  public function setDirectories(array $directoryArray);



}
