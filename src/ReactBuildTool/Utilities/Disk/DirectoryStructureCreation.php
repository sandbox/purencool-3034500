<?php

namespace Drupal\react_forms\ReactBuildTool\Utilities\Disk;

/**
 * Class DirectoryStructureCreation builds the directory structure if
 * it does not exist.
 *
 * @package Drupal\webform_react_components\ReactBuildTool\Utilities\Disk
 */
class DirectoryStructureCreation implements DirectoryStructureCreationInterface {

  /**
   * @var array
   */
  private $directoryArray;


  /**
   * @inheritdoc
   */
  public function createDirectories() {

    if(!is_dir($this->directoryArray['react_forms_root'])){
      \Drupal::service('file_system')->mkdir($this->directoryArray['react_forms_root']);
    }

    foreach ( $this->directoryArray as $formArr) {
      foreach($formArr as $value) {
        \Drupal::service('file_system')->mkdir($value);
      }
    }
  }


  /**
   * @inheritdoc
   *
   * @todo remove all files from the path created by code
   */
  public function deleteDirectories() {
    \Drupal::service('file_system')
      ->rmdir('/var/beetbox/public_html/sites/default/files/react_forms');
  }


  /**
   * @inheritdoc
   */
  public function setDirectories(array $directoryArray) {
    $this->directoryArray = $directoryArray;
  }
}
