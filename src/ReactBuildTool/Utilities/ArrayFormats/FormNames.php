<?php

namespace Drupal\react_forms\ReactBuildTool\Utilities\ArrayFormats;

/**
 *
 * Class FormNames
 *
 * @package Drupal\react_forms\ReactBuildTool\Utilities\ArrayFormats
 */
class FormNames {

  /**
   *  Takes first level of are any forms them into an array that
   *  will be the machine names for the
   *
   * @param array $formArray
   *
   * @return array
   */
   public static function formNames(array $formArray){
     $return = [];
     $array = array_map(function ($v){if (is_array($v) || is_object($v)) {return "";}return $v;}, $formArray);
     foreach($array as $value){
       if($value !==  ''){
         $return[] = $value;
       }
     }
      return $return;
   }
}
