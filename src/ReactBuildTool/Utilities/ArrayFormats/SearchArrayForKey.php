<?php

namespace Drupal\react_forms\ReactBuildTool\Utilities\ArrayFormats;

/**
 * Class SearchArrayForKey
 *
 * @package Drupal\react_forms\ReactBuildTool\Utilities\ArrayFormats
 */
class SearchArrayForKey {

  /**
   * @param array $arr
   *
   * @return array
   */
   public static function search(array $arr, $search){
     $arrIt = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($arr));

     foreach ($arrIt as $key => $sub) {
       $subArray = $arrIt->getSubIterator();
       if ($subArray[$key] === $search) {
         $outputArray[] = iterator_to_array($subArray);
         break;
       }
     }

     return $outputArray;
   }
}
