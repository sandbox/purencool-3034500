<?php
namespace Drupal\react_forms\ReactBuildTool\Utilities\Files;

/**
 * Class FileCreation
 *
 * @package Drupal\react_forms\ReactBuildTool\Utilities\Files
 */
class FileCreation {

  /**
   * @todo make creation cleaner and document
   * @param $path
   * @param $name
   * @param $extension
   * @param $result
   * @return Null
   */
  public static function create($path,$name,$extension,$result){

    file_put_contents("$path/$name.$extension", $result);
  }
}
