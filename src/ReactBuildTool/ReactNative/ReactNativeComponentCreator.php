<?php

namespace Drupal\react_forms\ReactBuildTool\ReactNative;

use Drupal\react_forms\ReactBuildTool\React\Templates\RenderTemplate;
use Drupal\react_forms\ReactBuildTool\React\Templates\ConstructorTemplate;
use Drupal\react_forms\ReactBuildTool\React\Templates\FooterTemplate;
use Drupal\react_forms\ReactBuildTool\React\Templates\HeaderTemplate;
use Drupal\react_forms\ReactBuildTool\Utilities\Files\FileCreation;



/**
 * Class ReactBase.
 */
class ReactNativeComponentCreator {

  /**
   * @var \Drupal\react_forms\ReactBuildTool\React\Templates\HeaderTemplate
   */
  private $headerTemplate;

  /**
   * @var \Drupal\react_forms\ReactBuildTool\React\Templates\ConstructorTemplate
   */
  private $constructorTemplate;

  /**
   * @var \Drupal\react_forms\ReactBuildTool\React\Templates\RenderTemplate
   */
  private $renderTemplate;

  /**
   * @var \Drupal\react_forms\ReactBuildTool\React\Templates\FooterTemplate
   */
  private $footerTemplate;


  /**
   * @var array
   */
  private $config;


  /**
   * @var array
   */
  private static $formElementTypes = [
    'button',
    'checkbox',
    'color',
    'date',
    'datetime-local',
    'email',
    'file',
    'hidden',
    'image',
    'month',
    'number',
    'password',
    'radio',
    'range',
    'reset',
    'search',
    'submit',
    'tel',
    'text',
    'time',
    'url',
    'week',
    'form',
    'textarea',
    'label',
    'fieldset',
    'legend',
    'select',
    'optgroup',
    'option',
    'datalist',
    'output',
  ];


  /**
   * ComponentCreator constructor.
   *
   * @example
   *  $config = [
   *   'directory' => ,
   *   'file_extension' => ,
   *   'form_element_name' => ,
   *   'form_element_type' => ,
   *  ];
   *
   *
   * @param array $config
   */
  public function __construct(array $config) {
    $this->config = $config;
    $this->headerTemplate = new HeaderTemplate();
    $this->constructorTemplate = new ConstructorTemplate();
    $this->renderTemplate = new RenderTemplate();
    $this->footerTemplate = new FooterTemplate();

    $this->buildComponent();

  }


  /**
   *
   */
  public function buildComponent() {
    foreach (self:: $formElementTypes  as $elementType) {


    if ($this->config['form_element_type'] === $elementType) {
        $className =str_replace(' ', '',  ucwords($this->config['form_element_name']).ucwords($this->config['form_element_type']));
        $result = $this->headerTemplate::templateStr($className);
        $result .= $this->constructorTemplate::templateStr();

        $attributes['text'] = $this->config['form_element_name'];
        $staticClass = ucwords($this->config['form_element_type']);
        $staticClassName = "\\Drupal\\react_forms\\ReactBuildTool\\ReactNative\\Templates\\Elements\\$staticClass";
        $inputString =  $staticClassName::elementCreator($attributes);
        $result .= $this->renderTemplate::templateStr( $inputString );
        $result .= $this->footerTemplate::templateStr();

       // print "<pre>";
       // print_r($this->config);
       // print "</pre>";

        FileCreation::create(
          $this->config['directory'],
          $className,
          $this->config['file_extension'],
          $result
        );

      }
    }
  }


}
