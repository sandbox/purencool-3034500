<?php


namespace Drupal\react_forms\ReactBuildTool\ReactNative\Templates;

/**
 * Class HeaderTemplate
 *
 * @package Drupal\react_forms\React\Templates
 */
class ConstructorTemplate {
  public static function templateStr() {
    return <<<EOT
    
   constructor(props) {
     super(props);
       this.state = {
         value: null,
      };
   }
EOT;
  }
}
