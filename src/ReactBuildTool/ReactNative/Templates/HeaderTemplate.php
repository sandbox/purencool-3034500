<?php


namespace Drupal\react_forms\ReactBuildTool\ReactNative\Templates;

/**
 * Class HeaderTemplate
 *
 * @package Drupal\react_forms\React\Templates
 */
class HeaderTemplate {

  /**
   * @param  string $className
   *
   * @return string
   */
  public static function templateStr($className) {

    return  <<<EOT
class $className extends React.Component {
EOT;
  }
}
