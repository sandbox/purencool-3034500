<?php

namespace Drupal\react_forms\ReactBuildTool\ReactNative\Templates\Elements;



/**
 * Class Inputs.
 */
class Fieldset implements InputInterface{

  /**
   * @inheritdoc
   */
  public static function elementCreator(array $attributes){
    $text = $attributes['text'];
    return <<<EOT

         <fieldset>
           <legend>$text</legend>
        </fieldset>
EOT;
  }
}
