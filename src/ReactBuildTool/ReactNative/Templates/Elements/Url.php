<?php

namespace Drupal\react_forms\ReactBuildTool\ReactNative\Templates\Elements;



/**
 * Class Inputs.
 */
class Url implements InputInterface{

  /**
   * @inheritdoc
   */
  public static function elementCreator(array $attributes){
    $text = $attributes['text'];
    $name =  strtolower(str_replace(' ', '',  $attributes['text']));
    return <<<EOT
         <label>
           $text
          <input name="$name" id="$name" type="radio" />
        </label>
EOT;
  }
}
