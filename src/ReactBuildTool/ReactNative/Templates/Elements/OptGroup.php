<?php

namespace Drupal\react_forms\ReactBuildTool\ReactNative\Templates\Elements;



/**
 * Class Inputs.
 */
class OptGroup implements InputInterface{

  /**
   * @inheritdoc
   */
  public static function elementCreator(array $attributes){
    $text = $attributes['text'];
    $name =  strtolower(str_replace(' ', '',  $attributes['text']));
    return <<<EOT
    
         <optgroup label="$text"  >
         </optgroup>

EOT;
  }
}
