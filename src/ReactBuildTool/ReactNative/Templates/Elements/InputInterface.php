<?php

namespace Drupal\react_forms\ReactBuildTool\ReactNative\Templates\Elements;



/**
 * Class Inputs.
 */
interface InputInterface {

  /**
   * @param array $attributes
   *
   * @return string
   */
  public static function elementCreator(array $attributes);
}
