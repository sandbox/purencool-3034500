<?php


namespace Drupal\react_forms\ReactBuildTool\ReactNative\Templates;

/**
 * Class HeaderTemplate
 *
 * @package Drupal\react_forms\React\Templates
 */
class RenderTemplate {

  /**
   * @param string $inputType
   *
   * @return string
   */
  public static function templateStr($inputType) {

    return <<<EOT
    
    render() {
      return (
       $inputType
      );
    }
EOT;
  }
}

