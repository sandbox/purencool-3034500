<?php


namespace Drupal\react_forms\ReactBuildTool;


/**
 * Interface ReactBuildToolInterface
 *
 * @package Drupal\react_forms\ReactBuildTool
 */
interface ReactBuildToolInterface{

  /**
   * @param $directories
   * @param $fileExtension
   * @param $formsArray
   *
   * @return void
   */
   public function  buildReactNative($directories, $fileExtension,$formsArray);

  /**
   * @param $directories
   * @param $fileExtension
   * @param $formsArray
   *
   * @return void
   */
  public function  buildReact($directories, $fileExtension,$formsArray);



}
