<?php

namespace Drupal\react_forms\ReactBuildTool\React\Templates\Elements;



/**
 * Class Inputs.
 */
class Password implements InputInterface {

  /**
   * @inheritdoc
   */
  public static function elementCreator(array $attributes){
    $text = $attributes['text'];
    $name =  strtolower(str_replace(' ', '',  $attributes['text']));
    return <<<EOT

         <label>
           $text
          <input name="$name" type="password"  />
        </label>
EOT;
  }
}
