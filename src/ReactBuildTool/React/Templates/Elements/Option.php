<?php

namespace Drupal\react_forms\ReactBuildTool\React\Templates\Elements;



/**
 * Class Inputs.
 */
class Option implements InputInterface{

  /**
   * @inheritdoc
   */
  public static function elementCreator(array $attributes){
    $text = $attributes['text'];
    $name =  strtolower(str_replace(' ', '',  $attributes['text']));
    return <<<EOT
         <option name="$name" value="$name" >$text </option>
EOT;
  }
}
