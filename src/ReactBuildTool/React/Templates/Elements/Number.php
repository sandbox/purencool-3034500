<?php

namespace Drupal\react_forms\ReactBuildTool\React\Templates\Elements;



/**
 * Class Inputs.
 */
class Number implements InputInterface{

  /**
   * @inheritdoc
   */
  public static function elementCreator(array $attributes){
    $text = $attributes['text'];
    $name =  strtolower(str_replace(' ', '',  $attributes['text']));
    return <<<EOT
         <label>
           $text
          <input name="$name" id="$name" type="number" />
        </label>
EOT;
  }
}
