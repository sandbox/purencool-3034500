<?php

namespace Drupal\react_forms\ReactBuildTool\React\Templates\Elements;



/**
 * Class Inputs.
 */
class Image implements InputInterface{

  /**
   * @inheritdoc
   */
  public static function elementCreator(array $attributes){
    $text = $attributes['text'];
    $name =  strtolower(str_replace(' ', '',  $attributes['text']));
    return <<<EOT
    
          <input name="$name"  id=" $name " type="image"  alt="$text"/>
EOT;
  }
}
