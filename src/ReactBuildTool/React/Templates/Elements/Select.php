<?php

namespace Drupal\react_forms\ReactBuildTool\React\Templates\Elements;



/**
 * Class Inputs.
 */
class Select implements InputInterface{

  /**
   * @inheritdoc
   */
  public static function elementCreator(array $attributes){
    $text = $attributes['text'];
    $name =  strtolower(str_replace(' ', '',  $attributes['text']));
    return <<<EOT
    
       <div class="field--item">
         <label>$text </label>
         <select name="$name" id="$name" >
         </select>
       </div>   
      
EOT;
  }
}
