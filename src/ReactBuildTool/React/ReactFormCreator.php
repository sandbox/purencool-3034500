<?php

namespace Drupal\react_forms\ReactBuildTool\React;


/**
 * Class ReactBase.
 */
class ReactFormCreator {

  /**
   * @todo make it so the templating system is attached to an array so it
   *  can be overridden
   *
   * @example
   *  $config = [
   *   'directory' => 'String: path file is going to be created',
   *   'file_extension' => 'String: file extension',
   *   'form_element_name' => 'String: form name',
   *   'form_element_type' => 'Array: form elements to be parsed',
   *   'templating' => Array: That has objects attached so you can override the defaults
   * ];
   *
   * @param $config
   * @param $type
   * @param $name
   * @param $templating
   */
  protected function createComponent($config, $type, $name,$templating) {

    $config = [
      'directory' => $config['directory'],
      'file_extension' => $config['file_extension'],
      'form_element_name' => $name,
      'form_element_type' => $type,
      'templating' => $templating
    ];

    new ReactComponentCreator($config);

  }


  /***
   *
   * @param array $config
   */
  public function setBuilder(array $config) {

    foreach ($config['form_elements'] as $formElements) {
      if ($formElements['#type'] === 'fieldset') {
        foreach ($formElements as $innerFormElements) {
          $this->createComponent($config, $innerFormElements['#type'], $innerFormElements['#title'],$config['templating']);
        }
        $this->createComponent($config, $formElements['#type'], $formElements['#title'], $config['templating']);
      } else {
        $this->createComponent($config, $formElements['#type'], $formElements['#title'], $config['templating']);
      }

    }
  }
}
