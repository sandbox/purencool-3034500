<?php
namespace Drupal\react_forms\ReactBuildTool;


use Drupal\react_forms\ReactBuildTool\Utilities\ArrayFormats\FormNames;
use Drupal\react_forms\ReactBuildTool\React\ReactFormCreator;
use Drupal\react_forms\ReactBuildTool\ReactNative\ReactNativeFormCreator;



/**
 * Class ReactBuildTool
 *
 * @package Drupal\react_forms\ReactBuildTool
 */
class ReactBuildTool implements ReactBuildToolInterface{


  /**
   * @inheritdoc
   */
  public function  buildReactNative($directories, $fileExtension,$formsArray, $templates = []){


    foreach(FormNames::formNames($formsArray) as $formNameValue){
      $obj = new ReactNativeFormCreator();
      $config = [
        'directory' => $directories[$formNameValue]['react_native_components'],
        'form_name' => $formNameValue,
        'file_extension' => $fileExtension->extension,
        'form_elements' => $formsArray[$formNameValue],
        'templating' => $templates
      ];
      $obj->setBuilder($config);
    }
  }


  /**
   * @inheritdoc
   */
  public function  buildReact($directories, $fileExtension,$formsArray, $templates = []){

    foreach(FormNames::formNames($formsArray) as $formNameValue){
      $obj = new ReactFormCreator();
      $config = [
        'directory' => $directories[$formNameValue]['react_components'],
        'form_name' => $formNameValue,
        'file_extension' => $fileExtension->extension,
        'form_elements' => $formsArray[$formNameValue],
        'templating' => $templates
      ];
      $obj->setBuilder($config);
    }
  }

}
