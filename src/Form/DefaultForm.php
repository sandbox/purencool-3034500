<?php

namespace Drupal\react_forms\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\react_forms\ReactFormsInit;


/**
 * Class DefaultForm.
 */
class DefaultForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    ReactFormsInit::createDirectories();
    ReactFormsInit::buildAllForms();

    $form['react_path_components'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path components are going to saved'),
      '#maxlength' => 128,
      '#size' => 128,
      '#weight' => '0',
      '#value' => ReactFormsInit::directory()['react_forms_root'].'/react_forms',
      '#attriubtes' => [
        'readonly'
      ]
    ];
   // $form['submit'] = [
   //   '#type' => 'submit',
   //   '#value' => $this->t('Submit'),
   // ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }

  }

}
