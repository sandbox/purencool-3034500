<?php
namespace Drupal\react_forms\Store;

/**
 * Interface ConfigurationObjectInterface
 *
 * @package Drupal\react_forms\Store
 */
interface ConfigurationObjectInterface {

  /**
   * @return array
   */
  public function getConfig();

}
