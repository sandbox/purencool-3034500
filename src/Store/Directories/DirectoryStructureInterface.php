<?php

namespace Drupal\react_forms\Store\Directories;

/**
 * Interface DirectoryStructureInterface
 *
 * @package Drupal\react_forms\Directories\Store
 */
interface DirectoryStructureInterface{


  /**
   * @param array $formsArray
   *
   * @return mixed
   */
  public function getDirectories(array $formsArray);


  /**
   * @param string $path
   *
   * @return void
   */
  public function setBasePath();


  /**
   * @param  string $directories
   *
   * @return void
   */
  public function setDirectories($directories);

}
