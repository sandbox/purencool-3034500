<?php

namespace Drupal\react_forms\Store\Directories;

use Drupal\react_forms\ReactBuildTool\Utilities\ArrayFormats\FormNames;
use Drupal\Core\File\FileSystemInterface;


/**
 * Class DirectoryStructure
 *
 * @package Drupal\react_forms\Directories\Store
 */
class DirectoryStructure implements DirectoryStructureInterface {


  /**
   * @var array
   */
  private $directories = [

    'root' => [
      'dir_name' => '',
      [
        'react_forms' => '/react_forms',
        [
          'react_native' => '/react_native',
          [
            'components' => '/components',
          ],
        ],
        [
          'react' => '/react',
          [
            'dir_name' => '/components',
          ],
        ],
      ],
    ],
  ];


  /**
   * The file system service.
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;




  /**
   * DirectoryStructure constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
    $this->setBasePath();
  }







  private function arrayFlattenRecursive($array) {
    if (!$array) {
      return FALSE;
    }
    $flat = [];
    $RII = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($array));
    foreach ($RII as $value) {
      $flat[] = $value;
    }
    return $flat;
  }


  /**
   * @inheritdoc
   * @todo This needs to be cleaned up it seems that a token system is more
   * important for the development of the form components
   * than complete decoupling
   */
  public function getDirectories(array $formsArray) {

    $root = $this->directories['root']['dir_name'];
    $subDirectoryUrl = FormNames::formNames($formsArray);
    $arr = [
      'react_forms_root' => "$root/react_forms/"
    ];
    foreach ($subDirectoryUrl as $value) {
      $arr [$value] = [
        'react_home' => "$root/react_forms/$value",
        'react_native' => "$root/react_forms/$value/react_native",
        'react_native_components' => "$root/react_forms/$value/react_native/components/",
        'react' => "$root/react_forms/$value/react",
        'react_components' => "$root/react_forms/$value/react/components",
      ];
    }

    return $arr; //$this->arrayFlattenRecursive($arr);
  }


  /**
   * @inheritdoc
   */
  public function setBasePath() {
      $this->directories['root']['dir_name'] = $this->fileSystem->realpath(file_default_scheme() . "://");
  }


  /**
   * @inheritdoc
   */
  public function setDirectories($directories) {
    $this->directories = $directories;
  }

}
