<?php

namespace Drupal\react_forms\Store\FormArray;

/**
 * Class FormRenderArray
 *
 * @package Drupal\react_forms\FormArray\Store
 */
class FormRenderArray implements FormRenderArrayInterface {

  /**
   *
   * @example
   *  $array = [
   *   'form_name' => 'unique id',
   *     [ form_element => 'element type',
   *        [
   *          element attributes
   *        ]
   *     ]
   *  ];
   *
   * @var array
   */
  private $forms = [];


  /**
   * FormRenderArray constructor.
   *
   * @param array $formArray
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $formArray = []) {
    $this->formArraySetup($formArray);
  }


  /**
   * @param array $formArray
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function formArraySetup(array $formArray) {

    if (!empty($formArray)) {
      array_merge($this->forms, $formArray);
    }
    else {
      $entities = \Drupal::entityTypeManager()
        ->getStorage('webform')
        ->loadMultiple(NULL);
      foreach ($entities as $entity_id => $entity) {
        if ($entity->access('create', NULL)) {
          $this->forms[] = $entity_id;
          $formArray = \Drupal::entityTypeManager()
            ->getStorage('webform')
            ->load($entity_id);
          $formArrayResult = $formArray->getSubmissionForm();
          $this->forms[$entity_id] = $formArrayResult['elements'];
        }
      }
    }
  }


  /**
   * @inheritdoc
   */
  public function getFormsArray() {
    return $this->forms;
  }

}
