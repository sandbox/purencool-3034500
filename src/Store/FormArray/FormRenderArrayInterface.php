<?php

namespace Drupal\react_forms\Store\FormArray;

/**
 * Interface FormRenderArrayInterface
 *
 * @package Drupal\react_forms\FormArray\Store
 */
interface FormRenderArrayInterface {

  /**
   * @return array
   */
  public function getFormsArray();


}
