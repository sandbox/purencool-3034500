<?php
namespace Drupal\react_forms\Store;


use Drupal\react_forms\Store\Directories\DirectoryStructure;
use Drupal\react_forms\Store\FormArray\FormRenderArray;

/**
 * Class ConfigurationObject
 *
 * @package Drupal\react_forms\Store
 */
class ConfigurationObject implements ConfigurationObjectInterface{


  /**
   * @inheritdoc
   */
  public function getConfig(){
    return [
      'directory' => \Drupal::service('react_forms.react_forms_services'),
      'forms_array' => new FormRenderArray(),
      'file_extension' => (object) array('extension' => 'js'),
    ];
  }
}
